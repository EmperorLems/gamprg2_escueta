// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Gmprg2_EndlessRunner : ModuleRules
{
	public Gmprg2_EndlessRunner(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
