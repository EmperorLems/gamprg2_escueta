// Copyright Epic Games, Inc. All Rights Reserved.

#include "Gmprg2_EndlessRunner.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Gmprg2_EndlessRunner, "Gmprg2_EndlessRunner" );
 