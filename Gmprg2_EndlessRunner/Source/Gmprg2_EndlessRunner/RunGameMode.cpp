// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "Math/TransformNonVectorized.h"
#include "TimerManager.h"
#include "MyCharacter.h"


ARunGameMode::ARunGameMode()
{
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (AMyCharacter* myCharacter = Cast<AMyCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		myCharacter->OnDeath.AddDynamic(this, &ARunGameMode::OnPlayerDeath);
	};

	for (int i = 0; i < 5.0f; i++)
	{
		SpawnTile();
	}
}

void ARunGameMode::OnTileExit(ATile* cTile)
{	
	SpawnTile();

}

void ARunGameMode::OnPlayerDeath(AMyCharacter* MyCharacter)
{
	PlayerDeath(MyCharacter);
}

void ARunGameMode::SpawnTile()
{
	if (!Tile) return;

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	ATile* mTile = GetWorld()->SpawnActor<ATile>(Tile, NextSpawnLoc);
	NextSpawnLoc = mTile->GetAttachTransform();

	mTile->Exited.AddDynamic(this, &ARunGameMode::OnTileExit);
}

void ARunGameMode::DestroyTile(ATile* mTile)
{
	//mTile->Destroy();
	//GetWorld()->GetTimerManager().ClearTimer(TimerDelayHandle);
}
