// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_ENDLESSRUNNER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
private:
	class AMyCharacter* MyCharacter;

public:
        AMyPlayerController();
protected:
	virtual void BeginPlay() override;

    void MoveForward(float scale);
    void MoveRight(float scale);

public:
    virtual void Tick(float DeltaTime) override;

	virtual void SetupInputComponent() override;

	
};
