// Copyright Epic Games, Inc. All Rights Reserved.

#include "Gmprg2_EndlessRunnerGameMode.h"
#include "Gmprg2_EndlessRunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGmprg2_EndlessRunnerGameMode::AGmprg2_EndlessRunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
