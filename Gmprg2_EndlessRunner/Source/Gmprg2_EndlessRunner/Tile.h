	// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, class ATile*, cTile);

UCLASS()
class GMPRG2_ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
private:
		
	FTimerHandle TimerDelayHandle;

public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray <AActor> Actor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray < TSubclassOf<class AObstacle> > Obstacle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray < TSubclassOf<class APickup> > Pickup;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* ObstacleSpawnArea;
	
	class AObstacle* spawnedObstacle;
	class APickup* spawnedPickup;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(BlueprintAssignable)
		FExited Exited;

	UFUNCTION(BlueprintCallable)
		FTransform GetAttachTransform();

	UFUNCTION()
		void DestroyTile();

	UFUNCTION()
		void DestroyObstacle();

	UFUNCTION()
		void DestroyPickup();

	UFUNCTION()
		void SpawnObstacles();

	UFUNCTION()
		FVector GenerateSpawnArea(UBoxComponent* SpawnArea);
};
