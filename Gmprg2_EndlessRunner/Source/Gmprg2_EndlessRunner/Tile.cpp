// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Math/TransformNonVectorized.h"
#include "MyCharacter.h"
#include "TimerManager.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
			
	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	AttachPoint->SetupAttachment(RootComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(RootComponent);

	ObstacleSpawnArea = CreateDefaultSubobject<UBoxComponent>("ObstacleSpawnArea");
	ObstacleSpawnArea->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);

	SpawnObstacles();
}

FTransform ATile::GetAttachTransform()
{
	return AttachPoint->GetComponentTransform();
}

void ATile::DestroyTile()
{
	this->Destroy();
	DestroyObstacle();
	DestroyPickup();
	GetWorld()->GetTimerManager().ClearTimer(TimerDelayHandle);
}

void ATile::DestroyObstacle()
{
	if (spawnedObstacle != NULL)
	{
		spawnedObstacle->Destroy();
	}
}

void ATile::DestroyPickup()
{
	if (spawnedPickup != NULL)
	{
		spawnedPickup->Destroy();
	}
}

void ATile::SpawnObstacles()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	int32 randomInt = FMath::FRandRange(0, Obstacle.Num()-1);
	int32 randomSpawns = FMath::FRandRange(0, 100);

	if (randomSpawns <= 30)
	{
		spawnedPickup = GetWorld()->SpawnActor<APickup>(Pickup[randomInt], GenerateSpawnArea(ObstacleSpawnArea), FRotator::ZeroRotator);
	}
	else
	{
		spawnedObstacle = GetWorld()->SpawnActor<AObstacle>(Obstacle[randomInt], GenerateSpawnArea(ObstacleSpawnArea), FRotator::ZeroRotator);
	}
}

FVector ATile::GenerateSpawnArea(UBoxComponent* SpawnArea)
{
	FVector transform;
	//transform.SetLocation(UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetComponentLocation(), ObstacleSpawnArea->GetScaledBoxExtent()));
	transform = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetComponentLocation(), ObstacleSpawnArea->GetScaledBoxExtent());
	return transform;
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AMyCharacter* myCharacter = Cast<AMyCharacter>(OtherActor)) 
	{
		Exited.Broadcast(this);

		FTimerDelegate TimerDelegate;
		TimerDelegate.BindUFunction(this, FName("DestroyTile"));

		GetWorld()->GetTimerManager().SetTimer(TimerDelayHandle, TimerDelegate, 2.0f, false);
	}
}

