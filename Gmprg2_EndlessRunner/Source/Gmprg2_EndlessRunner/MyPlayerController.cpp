// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "MyCharacter.h"

AMyPlayerController::AMyPlayerController()
{
}

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();
	MyCharacter = Cast<AMyCharacter>(GetPawn());
}


void AMyPlayerController::MoveForward(float scale)
{
	MyCharacter->AddMovementInput(MyCharacter->GetActorForwardVector()* scale);
}

void AMyPlayerController::MoveRight(float scale)
{
	MyCharacter->AddMovementInput(MyCharacter->GetActorRightVector() * scale);
}

void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	InputComponent->BindAxis("MoveForward", this, &AMyPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayerController::MoveRight);
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	//InputComponent->BindAxis("MoveForward", this, &AMyPlayerController::MoveForward);
	//InputComponent->BindAxis("MoveRight", this, &AMyPlayerController::MoveRight);
}
