// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Gmprg2_EndlessRunnerGameMode.generated.h"

UCLASS(minimalapi)
class AGmprg2_EndlessRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGmprg2_EndlessRunnerGameMode();
};



