// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

private:

public:
	ARunGameMode();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FTransform NextSpawnLoc;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FTimerHandle TimerDelayHandle;
	
protected:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATile> Tile;

	virtual void BeginPlay() override;

	UFUNCTION()
		void OnTileExit(class ATile* cTile);

	UFUNCTION()
		void OnPlayerDeath(class AMyCharacter* MyCharacter);

	UFUNCTION(BlueprintImplementableEvent)
		void PlayerDeath(AMyCharacter* MyCharacter);

	UFUNCTION(BlueprintCallable)
	void SpawnTile();

	UFUNCTION()
		void DestroyTile(ATile* mTile);
};
