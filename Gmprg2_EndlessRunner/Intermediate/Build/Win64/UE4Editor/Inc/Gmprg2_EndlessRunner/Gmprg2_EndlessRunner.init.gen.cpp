// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGmprg2_EndlessRunner_init() {}
	GMPRG2_ENDLESSRUNNER_API UFunction* Z_Construct_UDelegateFunction_Gmprg2_EndlessRunner_OnDeath__DelegateSignature();
	GMPRG2_ENDLESSRUNNER_API UFunction* Z_Construct_UDelegateFunction_Gmprg2_EndlessRunner_Exited__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Gmprg2_EndlessRunner()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Gmprg2_EndlessRunner_OnDeath__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Gmprg2_EndlessRunner_Exited__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/Gmprg2_EndlessRunner",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xA169565E,
				0x89B0D8D5,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
