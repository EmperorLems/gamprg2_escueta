// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_ENDLESSRUNNER_MyPlayerController_generated_h
#error "MyPlayerController.generated.h already included, missing '#pragma once' in MyPlayerController.h"
#endif
#define GMPRG2_ENDLESSRUNNER_MyPlayerController_generated_h

#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_SPARSE_DATA
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_RPC_WRAPPERS
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyPlayerController(); \
	friend struct Z_Construct_UClass_AMyPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(AMyPlayerController)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyPlayerController(); \
	friend struct Z_Construct_UClass_AMyPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(AMyPlayerController)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPlayerController(AMyPlayerController&&); \
	NO_API AMyPlayerController(const AMyPlayerController&); \
public:


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPlayerController(AMyPlayerController&&); \
	NO_API AMyPlayerController(const AMyPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyPlayerController)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_PRIVATE_PROPERTY_OFFSET
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_12_PROLOG
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_RPC_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_INCLASS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_ENDLESSRUNNER_API UClass* StaticClass<class AMyPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
