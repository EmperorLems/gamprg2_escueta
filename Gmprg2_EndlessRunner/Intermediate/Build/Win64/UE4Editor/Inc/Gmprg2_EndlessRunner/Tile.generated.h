// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
class UBoxComponent;
struct FVector;
struct FTransform;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GMPRG2_ENDLESSRUNNER_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define GMPRG2_ENDLESSRUNNER_Tile_generated_h

#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_10_DELEGATE \
struct _Script_Gmprg2_EndlessRunner_eventExited_Parms \
{ \
	ATile* cTile; \
}; \
static inline void FExited_DelegateWrapper(const FMulticastScriptDelegate& Exited, ATile* cTile) \
{ \
	_Script_Gmprg2_EndlessRunner_eventExited_Parms Parms; \
	Parms.cTile=cTile; \
	Exited.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_SPARSE_DATA
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGenerateSpawnArea); \
	DECLARE_FUNCTION(execSpawnObstacles); \
	DECLARE_FUNCTION(execDestroyPickup); \
	DECLARE_FUNCTION(execDestroyObstacle); \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execGetAttachTransform); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGenerateSpawnArea); \
	DECLARE_FUNCTION(execSpawnObstacles); \
	DECLARE_FUNCTION(execDestroyPickup); \
	DECLARE_FUNCTION(execDestroyObstacle); \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execGetAttachTransform); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AttachPoint() { return STRUCT_OFFSET(ATile, AttachPoint); } \
	FORCEINLINE static uint32 __PPO__ExitTrigger() { return STRUCT_OFFSET(ATile, ExitTrigger); } \
	FORCEINLINE static uint32 __PPO__Obstacle() { return STRUCT_OFFSET(ATile, Obstacle); } \
	FORCEINLINE static uint32 __PPO__Pickup() { return STRUCT_OFFSET(ATile, Pickup); } \
	FORCEINLINE static uint32 __PPO__ObstacleSpawnArea() { return STRUCT_OFFSET(ATile, ObstacleSpawnArea); }


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_12_PROLOG
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_RPC_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_INCLASS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_INCLASS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_ENDLESSRUNNER_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
