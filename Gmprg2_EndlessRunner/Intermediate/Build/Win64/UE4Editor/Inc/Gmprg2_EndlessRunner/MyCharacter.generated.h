// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMyCharacter;
#ifdef GMPRG2_ENDLESSRUNNER_MyCharacter_generated_h
#error "MyCharacter.generated.h already included, missing '#pragma once' in MyCharacter.h"
#endif
#define GMPRG2_ENDLESSRUNNER_MyCharacter_generated_h

#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_10_DELEGATE \
struct _Script_Gmprg2_EndlessRunner_eventOnDeath_Parms \
{ \
	AMyCharacter* MyCharacter; \
}; \
static inline void FOnDeath_DelegateWrapper(const FMulticastScriptDelegate& OnDeath, AMyCharacter* MyCharacter) \
{ \
	_Script_Gmprg2_EndlessRunner_eventOnDeath_Parms Parms; \
	Parms.MyCharacter=MyCharacter; \
	OnDeath.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_SPARSE_DATA
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCoin); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execDie);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCoin); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execDie);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend struct Z_Construct_UClass_AMyCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyCharacter(); \
	friend struct Z_Construct_UClass_AMyCharacter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacter)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public:


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacter(AMyCharacter&&); \
	NO_API AMyCharacter(const AMyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCharacter)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(AMyCharacter, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(AMyCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__SkeletalMesh() { return STRUCT_OFFSET(AMyCharacter, SkeletalMesh); }


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_12_PROLOG
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_RPC_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_INCLASS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_INCLASS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_ENDLESSRUNNER_API UClass* StaticClass<class AMyCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_MyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
