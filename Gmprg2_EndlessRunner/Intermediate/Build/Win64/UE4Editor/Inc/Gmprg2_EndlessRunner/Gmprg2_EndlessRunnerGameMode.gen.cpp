// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gmprg2_EndlessRunner/Gmprg2_EndlessRunnerGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGmprg2_EndlessRunnerGameMode() {}
// Cross Module References
	GMPRG2_ENDLESSRUNNER_API UClass* Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_NoRegister();
	GMPRG2_ENDLESSRUNNER_API UClass* Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Gmprg2_EndlessRunner();
// End Cross Module References
	void AGmprg2_EndlessRunnerGameMode::StaticRegisterNativesAGmprg2_EndlessRunnerGameMode()
	{
	}
	UClass* Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_NoRegister()
	{
		return AGmprg2_EndlessRunnerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Gmprg2_EndlessRunner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Gmprg2_EndlessRunnerGameMode.h" },
		{ "ModuleRelativePath", "Gmprg2_EndlessRunnerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGmprg2_EndlessRunnerGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::ClassParams = {
		&AGmprg2_EndlessRunnerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGmprg2_EndlessRunnerGameMode, 742763932);
	template<> GMPRG2_ENDLESSRUNNER_API UClass* StaticClass<AGmprg2_EndlessRunnerGameMode>()
	{
		return AGmprg2_EndlessRunnerGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGmprg2_EndlessRunnerGameMode(Z_Construct_UClass_AGmprg2_EndlessRunnerGameMode, &AGmprg2_EndlessRunnerGameMode::StaticClass, TEXT("/Script/Gmprg2_EndlessRunner"), TEXT("AGmprg2_EndlessRunnerGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGmprg2_EndlessRunnerGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
