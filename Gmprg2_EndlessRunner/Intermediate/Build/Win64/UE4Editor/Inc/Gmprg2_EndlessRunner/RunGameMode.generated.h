// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
class AMyCharacter;
#ifdef GMPRG2_ENDLESSRUNNER_RunGameMode_generated_h
#error "RunGameMode.generated.h already included, missing '#pragma once' in RunGameMode.h"
#endif
#define GMPRG2_ENDLESSRUNNER_RunGameMode_generated_h

#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_SPARSE_DATA
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execSpawnTile); \
	DECLARE_FUNCTION(execOnPlayerDeath); \
	DECLARE_FUNCTION(execOnTileExit);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execSpawnTile); \
	DECLARE_FUNCTION(execOnPlayerDeath); \
	DECLARE_FUNCTION(execOnTileExit);


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_EVENT_PARMS \
	struct RunGameMode_eventPlayerDeath_Parms \
	{ \
		AMyCharacter* MyCharacter; \
	};


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_CALLBACK_WRAPPERS
#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gmprg2_EndlessRunner"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public:


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunGameMode)


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Tile() { return STRUCT_OFFSET(ARunGameMode, Tile); }


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_12_PROLOG \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_EVENT_PARMS


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_CALLBACK_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_INCLASS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_SPARSE_DATA \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_CALLBACK_WRAPPERS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
	Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GMPRG2_ENDLESSRUNNER_API UClass* StaticClass<class ARunGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gmprg2_EndlessRunner_Source_Gmprg2_EndlessRunner_RunGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
